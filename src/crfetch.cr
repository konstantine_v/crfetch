require "shell"
require "./crfetch/**"

module Fetch
  VERSION = "0.2.1"
  OS_G    = Fetch::OS.new.to_s

  puts Fetch::ASCII.new
  puts "Distro: #{Fetch::OS.distro_pretty}"

  case OS_G # Constant for what the OS is
  when "Linux"
    # cPU
    cpu = Fetch::CPU.new
    puts "CPU: #{cpu.cpu_info}"
    # puts "Physical Cores: #{cpu.cpu_core_phy}"
    # puts "Threads: #{cpu.cpu_core_log}"

    # Memory
    mem = Fetch::Memory.new
    puts "Total Memory: #{mem.total}"
    puts "Free Memory: #{mem.free}"
    puts "Used Memory: #{mem.used}"
  when "MacOS"

    # CPU
    cpu = Fetch::CPU.new
    puts "CPU: #{Shell.run(cpu.cpu_info)}"
    puts "Logical Cores: #{Shell.run(cpu.cpu_core_log)}"
    puts "Physical Cores: #{Shell.run(cpu.cpu_core_phy)}"

    # Memory
    mem = Fetch::Memory.new
    puts "Total Memory: #{Shell.run(mem.total).chomp} GB"
  else
    raise "OS Not Found" # Replace with cleaner exception handling
  end
end
