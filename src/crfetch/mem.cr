class Fetch::Memory
  # Initialize Variables used
  @@mem_file : Array(String) = File.read_lines("/proc/meminfo")[0..18]

  def total # Fetch::Memory.new.total
    case OS_G
    when "Linux"
      mem = @@mem_file[0].to_s.gsub("MemTotal:       ","").gsub(" kB","").to_i64
      mem = mem * 1024
      return mem.humanize
    when "MacOS"
      return "sysctl -n hw.memsize | while read i; do echo \"$i/1024/1024/1024\" | bc; done"
    else
      raise "OS Not Found"
    end
  end

  def free # Fetch::Memory.new.free
    case OS_G
    when "Linux"
      mem = @@mem_file[1].to_s.gsub("MemFree:       ","").gsub(" kB","").to_i64
      mem = mem * 1024
      return mem.humanize
    else
      raise "OS Not Found"
    end
  end

  def used # Fetch::Memory.new.used
    case OS_G
    when "Linux"
      mem_t = @@mem_file[0].to_s.gsub("MemTotal:       ","").gsub(" kB","").to_i64
      mem_t = mem_t * 1024
      mem_f = @@mem_file[1].to_s.gsub("MemFree:       ","").gsub(" kB","").to_i64
      mem_f = mem_f * 1024
      mem_used = mem_t - mem_f
      return mem_used.humanize
    else
      raise "OS Not Found"
    end
  end

end
