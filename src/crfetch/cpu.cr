class Fetch::CPU
  # Initialize Variables used
  @@cpu_file : Array(String) = File.read_lines("/proc/cpuinfo")[0..13]

  # Defining Methods
  def cpu_info # Fetch::CPU.new.cpu_info
    case OS_G
    when "Linux"
      return @@cpu_file[4].to_s.gsub("model name\t: ","")
    when "MacOS"
      return "sysctl -n machdep.cpu.brand_string"
    else
      raise "OS Not Found"
    end
  end

  def cpu_core_log # Fetch::CPU.new.cpu_core_log
    case OS_G
    when "Linux"
      return @@cpu_file[10].to_s.gsub("siblings\t: ","")
    when "MacOS"
      return "sysctl -n hw.logicalcpu_max"
    else
      raise "OS Not Found"
    end
  end

  def cpu_core_phy # Fetch::CPU.new.cpu_core_phy
    case OS_G
    when "Linux"
      return @@cpu_file[12].to_s.gsub("cpu cores\t: ","")
    when "MacOS"
      return "sysctl -n hw.physicalcpu_max"
    else
      raise "OS Not Found"
    end
  end
end
